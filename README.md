# Case 19

The task was provided by Ingosstrach (Ингосстрах) insurance company

The main problem was that column names were obscure.

**The final result included:**

1. one-hot encoding

2. using logistic-regression for feature selection

3. over and Undersampling using SMOTEENN

4. training RandomForest. It showed the best results + The task materials contained a doc with RandomForest title

5. selecting threshold

Final f1 was rather poor only 0.69 with 0.83 accuracy
